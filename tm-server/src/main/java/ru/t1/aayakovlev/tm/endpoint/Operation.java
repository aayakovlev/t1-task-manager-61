package ru.t1.aayakovlev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.request.AbstractRequest;
import ru.t1.aayakovlev.tm.dto.response.AbstractResponse;
import ru.t1.aayakovlev.tm.exception.AbstractException;

@FunctionalInterface
public interface Operation<RQ extends AbstractRequest, RS extends AbstractResponse> {

    @NotNull
    RS execute(@NotNull final RQ request) throws AbstractException;

}
