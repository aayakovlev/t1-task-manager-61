package ru.t1.aayakovlev.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.aayakovlev.tm.model.User;

@Repository
@Scope("prototype")
public interface UserRepository extends BaseRepository<User> {

    @Nullable
    User findByLogin(@NotNull final String login);

    @Nullable
    User findByEmail(@NotNull final String email);

    boolean existsByLogin(@NotNull final String login);

    boolean existsByEmail(@NotNull final String email);

}
