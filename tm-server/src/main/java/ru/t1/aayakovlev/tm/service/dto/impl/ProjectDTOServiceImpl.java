package ru.t1.aayakovlev.tm.service.dto.impl;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aayakovlev.tm.dto.model.ProjectDTO;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.auth.AuthenticationException;
import ru.t1.aayakovlev.tm.exception.entity.EntityNotFoundException;
import ru.t1.aayakovlev.tm.exception.entity.ProjectNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.DescriptionEmptyException;
import ru.t1.aayakovlev.tm.exception.field.IdEmptyException;
import ru.t1.aayakovlev.tm.exception.field.NameEmptyException;
import ru.t1.aayakovlev.tm.exception.field.StatusEmptyException;
import ru.t1.aayakovlev.tm.repository.dto.ProjectDTORepository;
import ru.t1.aayakovlev.tm.service.dto.ProjectDTOService;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
public class ProjectDTOServiceImpl extends AbstractExtendedDTOService<ProjectDTO, ProjectDTORepository> implements ProjectDTOService {

    @Getter
    @NotNull
    @Autowired
    private ProjectDTORepository repository;

    @NotNull
    @Override
    @Transactional
    public ProjectDTO create(
            @Nullable final String userId,
            @Nullable final String name
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName(name);
        project.setUserId(userId);
        return getRepository().save(project);
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        return getRepository().save(project);
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @NotNull final ProjectDTO resultProject = findById(userId, id);
        resultProject.setStatus(status);
        update(resultProject);
        return resultProject;
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO update(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final ProjectDTO model = findById(userId, id);
        model.setName(name);
        model.setDescription(description);
        return update(userId, model);
    }

    @Override
    @Transactional
    public void clear(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        getRepository().deleteAllByUserId(userId);
    }

    @Override
    public long count(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        return getRepository().countByUserId(userId);
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return getRepository().existsByUserIdAndId(userId, id);
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        return getRepository().findAllByUserId(userId);
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll(@Nullable final String userId, @Nullable final Comparator<ProjectDTO> comparator)
            throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (comparator == null) return findAll(userId);
        @NotNull final Sort sort = getSort(comparator);
        return getRepository().findAllByUserId(userId, sort);
    }

    @NotNull
    @Override
    public ProjectDTO findById(@Nullable final String userId, @Nullable final String id) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final Optional<ProjectDTO> resultEntity = getRepository().findByUserIdAndId(userId, id);
        if (!resultEntity.isPresent()) throw new ProjectNotFoundException();
        return resultEntity.get();
    }

    @Override
    @Transactional
    public void remove(@Nullable final String userId, @Nullable final ProjectDTO model) throws AbstractException {
        if (model == null) throw new ProjectNotFoundException();
        removeById(model.getUserId(), model.getId());
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String id) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (!existsById(userId, id)) throw new ProjectNotFoundException();
        getRepository().deleteByUserIdAndId(userId, id);
    }

}
