package ru.t1.aayakovlev.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import ru.t1.aayakovlev.tm.endpoint.impl.AbstractEndpoint;
import ru.t1.aayakovlev.tm.service.LoggerService;
import ru.t1.aayakovlev.tm.service.PropertyService;
import ru.t1.aayakovlev.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

import static ru.t1.aayakovlev.tm.constant.ApplicationConstant.PID_FILENAME;

@Component
@NoArgsConstructor
public final class ServerBootstrap {

    @NotNull
    @Autowired
    public static ApplicationContext context;

    @NotNull
    @Autowired
    private AbstractEndpoint[] endpoints;

    @NotNull
    @Autowired
    private Backup backup;

    @Getter
    @NotNull
    @Autowired
    private PropertyService propertyService;

    @Getter
    @NotNull
    @Autowired
    private LoggerService loggerService;

    private void initEndpoints() {
        Arrays.stream(endpoints).forEach(this::registry);
    }

    private void initBackup() {
        if (propertyService.getBackupEnabled())
            backup.init();
    }

    private void initPID() {
        @NotNull final String filename = PID_FILENAME;
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        try {
            Files.write(Paths.get(filename), pid.getBytes());
            @NotNull final File file = new File(filename);
            file.deleteOnExit();
        } catch (@NotNull final IOException e) {
            e.printStackTrace();
        }
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getHost();
        @NotNull final String port = propertyService.getPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    public void start() {
        initPID();
        initEndpoints();
        initBackup();
        loggerService.initJmsLogger();
        loggerService.info(
                "___________   _____      _________                                       \n" +
                        "\\__    ___/  /     \\    /   _____/  ____  _______ ___  __  ____  _______ \n" +
                        "  |    |    /  \\ /  \\   \\_____  \\ _/ __ \\ \\_  __ \\\\  \\/ /_/ __ \\ \\_  __ \\\n" +
                        "  |    |   /    Y    \\  /        \\\\  ___/  |  | \\/ \\   / \\  ___/  |  | \\/\n" +
                        "  |____|   \\____|__  / /_______  / \\___  > |__|     \\_/   \\___  > |__|   \n" +
                        "                   \\/          \\/      \\/                     \\/         ");
        Runtime.getRuntime().addShutdownHook(new Thread(this::stop));
    }

    private void stop() {
        backup.stop();
        loggerService.info("*** APPLICATION SHUTTING DOWN ***");
    }

}
