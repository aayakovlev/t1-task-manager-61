package ru.t1.aayakovlev.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.aayakovlev.tm.dto.request.TaskBindToProjectRequest;
import ru.t1.aayakovlev.tm.event.ConsoleEvent;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

@Component
public final class TaskBindToProjectListener extends AbstractTaskListener {

    @NotNull
    public static final String DESCRIPTION = "Bind task to project.";

    @NotNull
    public static final String NAME = "task-bind-to-project";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@taskBindToProjectListener.name() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.print("Enter project id: ");
        @NotNull final String projectId = nextLine();
        System.out.print("Enter task id: ");
        @NotNull final String taskId = nextLine();

        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(getToken());
        request.setTaskId(taskId);
        request.setProjectId(projectId);

        taskEndpoint.bindTaskToProject(request);
    }

}
