package ru.t1.aayakovlev.tm.component;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.t1.aayakovlev.tm.event.ConsoleEvent;
import ru.t1.aayakovlev.tm.exception.system.CommandNotSupportedException;
import ru.t1.aayakovlev.tm.listener.AbstractListener;
import ru.t1.aayakovlev.tm.service.*;
import ru.t1.aayakovlev.tm.util.SystemUtil;
import ru.t1.aayakovlev.tm.util.TerminalUtil;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static ru.t1.aayakovlev.tm.constant.ApplicationConstant.*;

@Component
@NoArgsConstructor
@AllArgsConstructor
public final class ClientBootstrap {

    @NotNull
    @Autowired
    private FileScanner fileScanner;

    @NotNull
    @Autowired
    private LoggerService loggerService;

    @NotNull
    @Autowired
    private AbstractListener[] listeners;

    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;

    private void initFileScanner() {
        fileScanner.init();
    }

    private void initPID() {
        @NotNull final String filename = PID_FILENAME;
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        try {
            Files.write(Paths.get(filename), pid.getBytes());
            @NotNull final File file = new File(filename);
            file.deleteOnExit();
        } catch (@NotNull final IOException e) {
            e.printStackTrace();
        }
    }

    private boolean processArguments(@Nullable final String[] arguments) {
        if (arguments == null || arguments.length == EMPTY_ARRAY_SIZE) return false;
        @Nullable final String argument = arguments[FIRST_ARRAY_ELEMENT_INDEX];
        return processArgument(argument);
    }

    private boolean processArgument(@Nullable final String argument) {
        if (argument == null || argument.isEmpty()) return false;
        @Nullable final AbstractListener listener = getListenerByArgument(argument);
        if (listener != null) {
            publisher.publishEvent(listener);
            return true;
        }
        return false;
    }

    @Nullable
    private AbstractListener getListenerByArgument(@NotNull final String argument) {
        for (@NotNull final AbstractListener listener : listeners) {
            if (argument.equals(listener.getArgument())) return listener;
        }
        return null;
    }

    public void processCommand(@Nullable final String command) throws Exception {
        @Nullable final AbstractListener listener = getListenerByName(command);
        if (listener == null) throw new CommandNotSupportedException();
        listener.handler(new ConsoleEvent(command));
    }

    @Nullable
    private AbstractListener getListenerByName(@NotNull final String name) {
        for (@NotNull final AbstractListener listener : listeners) {
            if (name.equals(listener.getArgument())) return listener;
        }
        return null;
    }

    public void prepareStartup() {
        initPID();
        loggerService.info(
                "___________   _____    _________  .__   .__                   __   \n" +
                "\\__    ___/  /     \\   \\_   ___ \\ |  |  |__|  ____    ____  _/  |_ \n" +
                "  |    |    /  \\ /  \\  /    \\  \\/ |  |  |  |_/ __ \\  /    \\ \\   __\\\n" +
                "  |    |   /    Y    \\ \\     \\____|  |__|  |\\  ___/ |   |  \\ |  |  \n" +
                "  |____|   \\____|__  /  \\______  /|____/|__| \\___  >|___|  / |__|  \n" +
                "                   \\/          \\/                \\/      \\/        ");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        initFileScanner();
    }

    public void prepareShutdown() {
        fileScanner.stop();
        loggerService.info("*** APPLICATION SHUTTING DOWN ***");
    }

    public void run(@Nullable final String[] args) {
        if (processArguments(args)) return;
        prepareStartup();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                @NotNull final String command = readCommand();
                if (command.isEmpty()) {
                    continue;
                }
                loggerService.command("Executed command: " + command);
                publisher.publishEvent(new ConsoleEvent(command));
                System.out.println("[OK]");
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    @NotNull
    private String readCommand() {
        return TerminalUtil.nextLine();
    }

}

