package ru.t1.aayakovlev.tm.service;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.aayakovlev.tm.listener.EntityListener;

import javax.jms.*;

import static ru.t1.aayakovlev.tm.constant.ApplicationConstant.QUEUE;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class ReceiverService {

    @NotNull
    @Autowired
    private ConnectionFactory connectionFactory;

    @SneakyThrows
    public void receive(@NotNull @Autowired EntityListener entityListener) {
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();
        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Queue destination = session.createQueue(QUEUE);
        @NotNull final MessageConsumer messageConsumer = session.createConsumer(destination);
        messageConsumer.setMessageListener(entityListener);
    }

}
