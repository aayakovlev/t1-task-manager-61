package ru.t1.aayakovlev.tm.config;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import javax.jms.ConnectionFactory;

import static ru.t1.aayakovlev.tm.constant.ApplicationConstant.MONGO_URL;

@Configuration
@ComponentScan("ru.t1.aayakovlev.tm")
public class LoggerConfig {

    @Bean
    public ConnectionFactory factory() {
        @NotNull final ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(MONGO_URL);
        factory.setTrustAllPackages(true);
        return factory;
    }

}
