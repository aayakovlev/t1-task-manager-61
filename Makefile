clean:
	mvn clean

install:
	mvn install

install-no-tests:
	mvn install -DskipTests

clean-install:
	mvn clean install

clean-install-no-tests:
	mvn clean install -DskipTests

init-scheme:
	cd tm-server && mvn liquibase:update

img:
	docker build -t tm-server .\\tm-server\\. && \
	docker build -t tm-client .\\tm-client\\. && \
	docker build -t tm-logger .\\tm-logger\\. && \
	docker build -t tm-balancer .\\tm-balancer\\.

up:
	docker-compose up -d

down:
	docker-compose down

build-img-up:
	mvn clean install && \
	docker build -t tm-server .\\tm-server\\. && \
    docker build -t tm-client .\\tm-client\\. && \
    docker build -t tm-logger .\\tm-logger\\. && \
    docker build -t tm-balancer .\\tm-balancer\\. && \
    docker-compose up -d

build-nt-img-up:
	mvn clean install -DskipTests && \
	docker build -t tm-server .\\tm-server\\. && \
    docker build -t tm-client .\\tm-client\\. && \
    docker build -t tm-logger .\\tm-logger\\. && \
    docker build -t tm-balancer .\\tm-balancer\\. && \
    docker-compose up -d
